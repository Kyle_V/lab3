/**
 * Kyle Veloso
 * id: 1635447
 * 
 * NOTE: NOT JUnit! This class used to other test along coding the production code 
 */


package LinearAlgebra;



public class otherTestingClass {
    


    public static void main(String[] args) {
        

        Vector3d test = new Vector3d(1, 1, 2);
        Vector3d otherVector = new Vector3d(2, 3, 4);

        System.out.println(test.dotProduct(otherVector));
    }
    
}
