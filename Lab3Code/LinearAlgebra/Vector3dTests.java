/**
 * Kyle Veloso
 * id: 1635447
 * 
 * Used to test Vector3d class
 */
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTests {

    @Test
    //Test Getters of all Vector3d components
    public void testVector3dGetters() {
        Vector3d testVector = new Vector3d(-5, -1, 3.3);
        assertEquals(-5, testVector.getX());
        assertEquals(-1, testVector.getY());
        assertEquals(3.3, testVector.getZ());
    }

    @Test
    //Test getMagnitude of a vector3d
    public void testMagnitude() {
        Vector3d testVector = new Vector3d(-4, 6, 12);
        assertEquals(14, testVector.magnitude());
    }

    @Test
    //test dotProduct of a vector3d
    public void testDotProduct() {
        Vector3d testVectorA = new Vector3d(1, 1, 2);
        Vector3d testVectorB = new Vector3d(2, 3, 4);
        assertEquals(13, testVectorA.dotProduct(testVectorB));
    }

    @Test
    //test add method to see if the resultant matches expected
    public void testAdd() {
        Vector3d testVectorA = new Vector3d(1, 1.5, 2);
        Vector3d testVectorB = new Vector3d(-5, 0, 8);
        Vector3d resultant = testVectorA.add(testVectorB);

        //check if each component of resultant is the same as expected
        assertEquals(-4, resultant.getX());
        assertEquals(1.5, resultant.getY());
        assertEquals(10, resultant.getZ());
    }
}
