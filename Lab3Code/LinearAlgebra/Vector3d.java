/**
 * Kyle Veloso
 * id: 1635447
 * 
 * Class is a 3D Vector (immutable) in which Vector objects have x, y, and z components along with methods to add, get dot product and magnitude
 */

package LinearAlgebra;

public class Vector3d {

    private double x;   //x - component
    private double y;   //y - component
    private double z;   //z - component

    //constructor that makes a vector given x,y and z component inputs
    public Vector3d(double xComp, double yComp, double zComp) {
        this.x = xComp;
        this.y = yComp;
        this.z = zComp;

    }

    //getters
    public double getX() { return this.x; }
    public double getY() { return this.y; }
    public double getZ() { return this.z; }

    //get the magnitude of this 3d vector
    public double magnitude() {
        //formula: magnitude = sqrt(x^2 + y^2 + z^2)

        //get sum of squares, then take the sqrt
        double result = this.x*this.x + this.y*this.y + this.z*this.z;
        result = Math.sqrt(result);
        return result;
    }


    //get the dotProduct given input another vector
    public double dotProduct(Vector3d otherVector) {
        double dotProduct = (this.x * otherVector.getX()) + (this.y * otherVector.getY()) + (this.z * otherVector.getZ());
        return dotProduct;
    }


    //adds this vector given another Vector3 as an input and returns a resultant Vectoe3d
    public Vector3d add(Vector3d otherVector) {

        //adds the x components together and then creates a new vector
        double xComponentSum = this.x + otherVector.getX();
        double yComponentSum = this.y + otherVector.getY();
        double zCompomentSum = this.z + otherVector.getZ();
        Vector3d resultant = new Vector3d(xComponentSum, yComponentSum, zCompomentSum);

        return resultant;
    }

}